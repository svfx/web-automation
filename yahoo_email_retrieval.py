from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


browser = webdriver.Chrome()
browser.get("https://login.yahoo.com/")

login_user = browser.find_element_by_name("username")
login_user.send_keys($email, Keys.RETURN)

try:
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.ID, 'login-passwd'))
    )

finally:
    login_pass = browser.find_element_by_id('login-passwd')
    login_pass.send_keys($password, Keys.RETURN)


browser.get('https://mail.yahoo.com')

#CASE: open first message
## OPTIMIZE: loop through unread messages
unread_xpath = '/html/body/div[1]/div/div[1]/div/div[1]/div/div[1]/nav/div/div[3]/div[1]/ul/li[2]/a/div/span/span'
try:
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, unread_xpath))
    )

finally:
    unread_msgs = browser.find_element_by_xpath(unread_xpath)
    unread_msgs.click()

mail_xpath = '/html/body/div[1]/div/div[1]/div/div[1]/div/div[2]/div[1]/div/div/div[2]/div/div[2]/div/div/div[2]/ul[1]/li[2]/a/div[3]/div[1]'

try:
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, mail_xpath))
    )

finally:
    mail = browser.find_element_by_xpath (mail_xpath)
    mail.click()

print_button_xpath = '/html/body/div[1]/div/div[1]/div/div[1]/div/div[2]/div[1]/div/div[2]/div[2]/ul/li/div/header/div[3]/button/span'

try:
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, print_button_xpath))
    )

finally:
    print = browser.find_element_by_xpath(print_button_xpath).click()

try:
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.TAG_NAME, 'title'))
    )
finally:
    print_window = browser.find_element_by_tag_name("title").text
    browser.switch_to.window(print_window)
save_button = browser.find_element_by_class_name('action-button').click()

'''try:
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, 'action-button'))
    )

finally:
    save_button = browser.find_element_by_class_name('action-button').click()
'''


'''save_button = browser.find_element_by_xpath("//button[@class='action-button']")
save_button.send_keys(Keys.RETURN)
save_button.send_keys(Keys.RETURN)
'''
