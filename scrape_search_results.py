#! python3
# scrape_search_results.py is a selenium-based script that prints out links of results
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#go to search engine
browser = webdriver.Chrome()
browser.get('https://www.google.com/')

#type or "send" text to search bar
search_bar = browser.find_element_by_name('q').send_keys("what is food", Keys.RETURN)

#find results by links
results_list = browser.find_elements_by_tag_name('a')
browser.implicitly_wait(30)

print ("Found " + str(len(results_list)) + "results: ")

#print out results
i=0
for item in results_list:
    print (item.get_attribute('href'), '\n')
    if i == 10:
        break
    else:
        i+=1

browser.implicitly_wait(30)
browser.quit()
