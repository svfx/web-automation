# README #

### What is this repository for? ###

* Selenium-based Python scripts for practicing scraping. 
* Python version: 3

### How do I get set up? ###

* Laptop running on unix-based system.
* ```pip3 install -U selenium```
* I am using atom as an editor and terminal for testing, but you can use Pycharm, Wing, or any other IDE.


